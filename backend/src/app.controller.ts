import { Controller, Get, Query } from '@nestjs/common';
import { CharactersService } from './characters/characters.service';

@Controller()
export class AppController {
  constructor(private readonly charactersService: CharactersService) {}
  result:any;
  @Get()
  getHello(){
    return 'Hola Mundo!';
  }
  @Get('search')
  async searchCharacter(@Query('q') q: string ){
      await this.charactersService.findAll(q).then(value => this.result = value
      );
      let params = { slug: q }; 
      this.result =  await this.result.filter((item) => {
        for (let key in params) {
          let field = item[key];
          if (typeof field == 'string' && field.toString().toLowerCase().indexOf(params[key].toString().toLowerCase()) >= 0) {
            return item;
          } else if (field == params[key]) {
            return item;
          }
        }
        return null;
      });
      return this.result;
  }
}
