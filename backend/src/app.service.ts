import { Injectable, HttpService } from '@nestjs/common';
import { CharactersService } from './characters/characters.service';
import axios from 'axios';

@Injectable()
export class AppService {
  
  characters: any;

  constructor(private httpService: HttpService, private charactersService: CharactersService) {
    this.getAllCharacters();  
  }

  async getAllCharacters() {
    let url = 'https://api.got.show/api/show/characters';
    try {
      const response = await axios.get(url);
      response.data.map(character =>  this.charactersService.insertOne(character) );
      return true;
    } catch (exception) {
      process.stderr.write(`ERROR received from ${url}: ${exception}\n`);
      return false;
    }
  }

}
