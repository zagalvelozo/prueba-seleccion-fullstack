import { Injectable, HttpService } from '@nestjs/common';


@Injectable()
export class SeederService {
    constructor(
        private readonly httpService: HttpService,
    ) { }    
 
    async seed() {
        const url = 'https://api.got.show/api/general/characters';
        return await this.httpService.get(url);
    }

}