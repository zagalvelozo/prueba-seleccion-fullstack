import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, ObjectID } from 'typeorm';
import { Character } from './character.entity';
import {paginate, Pagination, IPaginationOptions} from 'nestjs-typeorm-paginate';

@Injectable()
export class CharactersService {
    constructor(
        @InjectRepository(Character)
        private readonly characterRepository: Repository<Character>,
    ) { }

    async paginate(options: IPaginationOptions): Promise<Pagination<Character>> {
        return paginate<Character>(this.characterRepository, options);
      }

    async findAll(q: string ): Promise<Character[]> {
        return this.characterRepository.find();
    }

    async findOne(id: ObjectID): Promise<Character> {
        return this.characterRepository.findOne({ "id": id });
    }

    async insertOne(char): Promise<Character[]> {
        return this.characterRepository.save(char);
    }

}
