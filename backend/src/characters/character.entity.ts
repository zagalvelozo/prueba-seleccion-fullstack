import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';

@Entity()
export class Character {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  name: String;

  @Column()
  gender: String;

  @Column()
  house: String;

  @Column()
  alive: Boolean;

  @Column()
  image: String;

  @Column()
  books: [String];

  @Column()
  culture: String;

  @Column()
  allegiance: [String];

  @Column()
  titles: [String];

  @Column()
  slug: String;
}
