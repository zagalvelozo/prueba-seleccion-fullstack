import { Controller, Get, Param, Query } from '@nestjs/common';
import { CharactersService } from './characters.service';
import { Character } from './character.entity';
import { Pagination } from 'nestjs-typeorm-paginate';

@Controller('characters')
export class CharactersController {

  constructor(private readonly charactersService: CharactersService) { }
  
  @Get('')
  async index(
    @Query('page') page: number = 1,
    @Query('limit') limit: number = 10,
  ): Promise<Pagination<Character>>{
    //limit = limit > 100 ? 100 : limit;
    return this.charactersService.paginate({
      page,
      limit,
      route: 'http://localhost:3000/api/characters',
    });
  }

  // @Get()
  // async findAll(): Promise<Character[]> {
  //   return this.charactersService.findAll();
  // }

  @Get(':id')
  async findOne(
    @Param() params
  ) {
    return this.charactersService.findOne(params.id);
  }

}
