import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Character } from './characters/character.entity';
import { CharacterModule } from './characters/character.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: 'localhost',
      database: 'test',
      entities: [Character],
      synchronize: true,
    }),
    CharacterModule,
    HttpModule
  ],
  controllers: [AppController],
  providers: [AppService ],
})
export class AppModule {}
