import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Data, Link, Meta } from './interfaces/data';
import { Character } from './character';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  private PROTOCOL = 'http://';
  private DNS = 'localhost:3000';
  private PATH = '/api/characters';
  private API = this.PROTOCOL +  this.DNS + '/api';
  private URL = this.PROTOCOL + this.DNS + this.PATH; // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService)  { }

  /** GET Character from the server */
  getCharacters(page: number): Observable<Data> {
    const url = `${this.URL}?page=${page}`;
    return this.http.get<Data>(url)
      .pipe(
        tap(_ => this.log('fetched Characters')),
        catchError(this.handleError<Data>('getCharacters'))
      );
  }

  getCharacter(id: string): Observable<Character> {
    const url = `${this.URL}/${id}`;
    return this.http.get<Character>(url).pipe(
      tap(_ => this.log(`fetched Character id=${id}`)),
      catchError(this.handleError<Character>(`getCharacter id=${id}`))
    );
  }

    /* GET Character whose name contains search term */
    searchCharacter(term: string): Observable<Character[]> {
      if (!term.trim()) {
        // if not search term, return empty hero array.
        return of([]);
      }
      return this.http.get<Character[]>(`${this.API}/search?q=${term}`).pipe(
        tap(x => x.length ?
           this.log(`found Characters matching "${term}"`) :
           this.log(`no Characters matching "${term}"`)),
        catchError(this.handleError<Character[]>('searchCharacters', []))
      );
    }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`CharacterService: ${message}`);
  }
}
