import { Character } from '../character';

export interface Data {
  items: Character[];
  meta: Meta;
  links: Link;
}

export interface Meta {
  totalItems: number;
  itemCount: number;
  itemsPerPage: number;
  totalPages: number;
  currentPage: string;
}

export interface Link {
  first: string;
  previous: string;
  next: string;
  last: string;
}
