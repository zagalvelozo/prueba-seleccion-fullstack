import { Component, OnInit } from '@angular/core';
import { Character } from '../character';
import { Data } from '../interfaces/data';
import { CharacterService } from '../character.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {

  data: Data;
  characters: Character[];
  loading = false;
  currentPage = 1;
  totalPages: Array<number> = [];
  constructor(private characterService: CharacterService) { }

   ngOnInit() {
    this.currentPage = 1;
    this.getCharacter(this.currentPage);
  }

  setPageTo(pageNumber) {
    this.currentPage = pageNumber;
    this.getCharacter(pageNumber);
  }

   getCharacter(page: number) {
    this.loading = true;
    this.characters = [];
    this.characterService.getCharacters(page)
      .subscribe(data => {
      this.characters = data.items;
      this.totalPages = Array.from(new Array(Math.ceil(data.meta.totalPages)), (val, index) => index + 1);
      }
    );
  }
}
