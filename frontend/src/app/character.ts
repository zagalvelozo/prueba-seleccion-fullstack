
export interface Character {
  id: string;
  name: string;
  slug: string;
  image: string;
  house: string;
  book: string;
  titles: string;
  rank: string;
  culture: string;
}

