import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import { Character } from '../character';

import { CharacterService } from '../character.service';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

@Component({
  selector: 'app-search-character',
  templateUrl: './search-character.component.html',
  styleUrls: ['./search-character.component.css']
})
export class SearchCharacterComponent implements OnInit {


  character$: Observable<Character[]>;

  private searchTerms = new Subject<string>();

  constructor(private characterService: CharacterService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.character$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.characterService.searchCharacter(term)),
    );
  }
}
